import { Injectable } from '@angular/core';
import { Connectivity } from './connectivity';
import { Geolocation } from 'ionic-native';
import 'rxjs/add/operator/map';

import 'rxjs/Rx';
import { Observable } from 'rxjs/Rx';

declare var google;
declare var GeoFire: any;

@Injectable()
export class GoogleMaps {

  mapElement: any;
  // pleaseConnect: any;
  map: any; driverMarker: any;
  mapInitialised: boolean = false;
  mapLoaded: any;
  mapLoadedObserver: any;
  markers: any = [];
  apiKey: 'AIzaSyAGrn5Vzz8VWHaISQJHNFa4EriTe9TD_S8';

  constructor(public connectivityService: Connectivity) {
    console.log('Hello GoogleMaps Provider');
  }

  init(mapElement: any): Promise<any> {

    this.mapElement = mapElement;
    // this.pleaseConnect = pleaseConnect;

    return this.loadGoogleMaps();

  }

  loadGoogleMaps(): Promise<any> {

    return new Promise((resolve) => {

      if (typeof google == "undefined" || typeof google.maps == "undefined") {

        console.log("Google maps JavaScript needs to be loaded.");
        this.disableMap();

        if (this.connectivityService.isOnline()) {

          window['mapInit'] = () => {

            this.initMap().then(() => {
              resolve(true);
            });

            this.enableMap();
          }

          let script = document.createElement("script");
          script.id = "googleMaps";

          if (this.apiKey) {
            script.src = 'http://maps.google.com/maps/api/js?key=' + this.apiKey + '&callback=initMap';
          } else {
            script.src = 'http://maps.google.com/maps/api/js?callback=mapInit';
          }

          document.body.appendChild(script);

        }
      }
      else {

        if (this.connectivityService.isOnline()) {
          this.initMap();
          this.enableMap();
        }
        else {
          this.disableMap();
        }

      }

      this.addConnectivityListeners();

    });

  }

  initMap(): Promise<any> {

    this.mapInitialised = true;

    return new Promise((resolve) => {

      Geolocation.getCurrentPosition().then((position) => {

        // UNCOMMENT FOR NORMAL USE
        let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

        // let latLng = new google.maps.LatLng(40.713744, -74.009056);

        let mapOptions = {
          center: latLng,
          scroll: true,
          rotate: true,
          zoomControl: false,
          disableDefaultUI: true,
          mapTypeControl: false,
          zoom: 15,
          tilt: 30,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        this.map = new google.maps.Map(this.mapElement, mapOptions);
        // this.addMarker(position.coords.latitude, position.coords.longitude)

        google.maps.event.addListener(this.map,'idle',function(){
          if(!this.get('dragging') && this.get('oldCenter') && this.get('oldCenter')!==this.getCenter()) {
            //do what you want to
            // myMarker.setPosition(this.getCenter());
            console.log(this.getCenter());
          }
          if(!this.get('dragging')){
            this.set('oldCenter',this.getCenter())
          }

        });

        google.maps.event.addListener(this.map,'dragstart',function(){
          this.set('dragging',true);
        });

        google.maps.event.addListener(this.map,'dragend',function(){
          this.set('dragging',false);
          google.maps.event.trigger(this,'idle',{});
        });
        resolve(true);

      });

    });

  }

  disableMap(): void {

    // if (this.pleaseConnect) {
    //   this.pleaseConnect.style.display = "block";
    // }

  }

  enableMap(): void {

    // if (this.pleaseConnect) {
    //   this.pleaseConnect.style.display = "none";
    // }

  }

  addConnectivityListeners(): void {

    document.addEventListener('online', () => {

      console.log("online");

      setTimeout(() => {

        if (typeof google == "undefined" || typeof google.maps == "undefined") {
          this.loadGoogleMaps();
        }
        else {
          if (!this.mapInitialised) {
            this.initMap();
          }

          this.enableMap();
        }

      }, 2000);

    }, false);

    document.addEventListener('offline', () => {

      console.log("offline");

      this.disableMap();

    }, false);

  }

  addMarker(lat: number, lng: number): void {

    let latLng = new google.maps.LatLng(lat, lng);

    let marker = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.DROP,
      position: latLng,
      icon: 'assets/img/pin.png'
    });

    // this.markers.push(marker);

  }

  rideMap(mapElement: any, pickuplat, pickuplng, driverLat, driverLng, driverId): Promise<any> {

    // this.mapInitialised = true;
    this.mapElement = mapElement;

    var directionsService = new google.maps.DirectionsService;
    var directionsDisplay = new google.maps.DirectionsRenderer;

    return new Promise((resolve) => {

      // UNCOMMENT FOR NORMAL USE
      // let latLng = new google.maps.LatLng(42.3600825, -71.05888010000001);
      let pickupLatLng = new google.maps.LatLng(pickuplat, pickuplng);
      let driverLatLng = new google.maps.LatLng(driverLat, driverLng);
      Geolocation.getCurrentPosition().then((pos) => {
        let mapOptions = {
          center: pickupLatLng,
          scroll: true,
          rotate: true,
          zoomControl: false,
          disableDefaultUI: true,
          mapTypeControl: false,
          zoom: 15,
          tilt: 30,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        this.map = new google.maps.Map(this.mapElement, mapOptions);
        this.driverMarker = new google.maps.Marker({
          map: this.map,
          position: driverLatLng,
          icon: 'assets/img/pin.png'
        });

      }).catch(err => console.log('stupid erro: ' + err));



      directionsDisplay.setMap(this.map);

      directionsService.route({
        origin: pickupLatLng,
        destination: driverLatLng,
        travelMode: 'DRIVING'
      }, function (response, status) {
        if (status === 'OK') {
          directionsDisplay.setDirections(response);
        } else {
          console.log('Directions request failed due to ' + status);
        }
      });

      let ref = firebase.database().ref('drivers/locations/' + driverId);
      ref.on("value", (snap) => {
        let val = snap.val();
        // this.driverLat = val.l[0];
        // this.driverLng = val.l[1];
        let markerPos = new google.maps.LatLng(val.l[0], val.l[1]);
        console.log('markerPos: ' + markerPos);
        this.driverMarker.setPosition(markerPos);

      });

      resolve(true);

    });

  }

  driverMarkerChanged(driverLat, driverLng) {

    let driverLatLng = new google.maps.LatLng(driverLat, driverLng);
    this.driverMarker.setPosition(driverLatLng)

  }
}
