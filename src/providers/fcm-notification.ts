import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the FcmNotification provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class FcmNotification {

  constructor(public http: Http) {
    console.log('Hello FcmNotification Provider');
  }
  sendNotificaiton(driverToken, clientToken, clientName, from, to, lat, lng, userUid) {
    console.log('driver from fcm: ');
    console.log(driverToken);
    console.log('client from fcm: ');
    console.log(clientToken);
    let headers = new Headers({'Authorization': 'key=AAAAYxwk15Y:APA91bHluZbx7hdDn4x_2MDROZeah-pWCjZnQYibHVLuDMNyCvCHOoOvHKYER6rSZ-m1dVydPDoLwvgjBsd7KoDwEgKPDtloeSKRqS9iflRU91K6e43-Vrfay1sw9q0IgLQN6aoxcNTyniwnaTao0xQBOhwLPmxZ0Q',
    'Content-Type': 'application/json'})
    let options = new RequestOptions({ headers: headers});
    let notifications = {
      "notification":{
        "title":"Ride Request",  //Any value
        "body":"Client near you requesting ride",  //Any value
        "sound":"default", //If you want notification sound
        "click_action":"FCM_PLUGIN_ACTIVITY",  //Must be present for Android
        "icon":"fcm_push_icon"  //White icon Android resource
      },
      "data":{
        "clientName": clientName,  //Any data to be retrieved in the notification callback
        "pickupLocation": from,
        "dropoffLocation": to,
        "lat": lat,
        "lng": lng,
        "From": clientToken,
        "userUid": userUid
      },
      "to": driverToken,
      "priority":"high", //If not set, notification won't be delivered on completely closed iOS app
      "restricted_package_name":"" //Optional. Set for application filtering
    };
    let body = JSON.stringify(notifications);

    return this.http.post('https://fcm.googleapis.com/fcm/send', body, options).subscribe( data => {
      console.log(data);
    });
    // console.log('Notification sent');
  }

}
