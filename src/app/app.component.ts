import { Component, ViewChild } from '@angular/core';
import { Platform, MenuController, Nav, LoadingController } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';
import { AuthData } from '../providers/auth-data';
// importing pages
// import { HomePage } from '../pages/home/home';
// import { ListPage } from '../pages/list/list';
// import { SignUpPage } from '..pages/sign-up/sign-up';
// import { PreorderRidePage } from '../pages/preorder-ride/preorder-ride';
import { MapPage } from '../pages/map/map';
import { LoginPage } from '../pages/login/login';
import { AboutPage } from '../pages/about/about';
import { PreorderRidePage } from '../pages/preorder-ride/preorder-ride';
import { ProfilePage } from '../pages/profile/profile';
import { RidePage } from '../pages/ride/ride';
import { ScheduledRidesPage } from '../pages/scheduled-rides/scheduled-rides';
import { RideHistoryPage } from '../pages/ride-history/ride-history';
import { IncentiveProgramsPage } from '../pages/incentive-programs/incentive-programs';

import { AngularFire } from 'angularfire2';
import { Keyboard } from 'ionic-native';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage: any;
  loading: any;
  public pages: Array<{title: string, component: any}>;

  constructor(platform: Platform,
    public af: AngularFire,
    public menuCtrl: MenuController,
    public loadingCtrl: LoadingController,
    public authData: AuthData) {

    Keyboard.hideKeyboardAccessoryBar(false);

    this.pages = [
      { title: 'Request a ride', component: MapPage},
      { title: 'Profile', component: ProfilePage},
      { title: 'About', component: AboutPage },
      { title: 'Pre-order ride', component: PreorderRidePage},
      { title: 'Scheduled rides', component: ScheduledRidesPage},
      { title: 'Ride history', component: RideHistoryPage},
      { title: 'Incentive programs', component: IncentiveProgramsPage}
    ];
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
      Splashscreen.hide();
      const authObserver = af.auth.subscribe( user => {
        if (user) {
          this.rootPage = MapPage;
          authObserver.unsubscribe();
        } else {
          this.rootPage = LoginPage;
          authObserver.unsubscribe();
        }
      });
      // this.rootPage = PreorderRidePage;
    });
  }
  openPage(page) {

    this.menuCtrl.close();
    this.nav.setRoot(page.component);
  }
  logoutUser() {
    this.menuCtrl.close();

    // this.authData.logoutUser();
    this.af.auth.logout();
    this.loading = this.loadingCtrl.create({
      dismissOnPageChange: true,
    });
    this.loading.present();
    // .then(() => {
    // }).catch(err => { console.log(err) })
    // this.nav.setRoot(LoginPage);
    // return this.af.auth.logout();
  }
}
