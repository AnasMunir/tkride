import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
// importing pages
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { ModalPage } from '../pages/modal-page/modal-page';
import { ResetPasswordPage } from '../pages/reset-password/reset-password';
import { SignUpPage } from '../pages/sign-up/sign-up';
import { MapPage } from '../pages/map/map';
import { ListPage } from '../pages/list/list';
import { AutocompletePage } from '../pages/autocomplete/autocomplete';
import { AutocompletedropoffPage } from '../pages/autocompletedropoff/autocompletedropoff';
import { ModalTestPagePage } from '../pages/modal-test-page/modal-test-page';
import { TermsAndConditionPage } from '../pages/terms-and-condition/terms-and-condition';
import { AboutPage } from '../pages/about/about';
import { PaymentPage } from '../pages/payment/payment';
import { PreorderRidePage } from '../pages/preorder-ride/preorder-ride';
import { ProfilePage } from '../pages/profile/profile';
import { EditProfilePage } from '../pages/edit-profile/edit-profile';
import { RidePage } from '../pages/ride/ride';
import { ScheduledRidesPage } from '../pages/scheduled-rides/scheduled-rides';
import { RideHistoryPage } from '../pages/ride-history/ride-history';
import { IncentiveProgramsPage } from '../pages/incentive-programs/incentive-programs'

// importing providers
import { AuthData } from '../providers/auth-data';
import { FcmNotification } from '../providers/fcm-notification';
import { AgmCoreModule } from 'angular2-google-maps/core';
import { Connectivity } from '../providers/connectivity';
import { GoogleMaps } from '../providers/google-maps';

// Import the AF2 Module
import { AngularFireModule, AuthProviders, AuthMethods } from 'angularfire2';

import { IonicImageLoader } from 'ionic-image-loader';

export const firebaseConfig = {
  apiKey: "AIzaSyAPl8CEm1VBFfaAZ-B0fY_CEyR3PUXmm-c",
  authDomain: "user-tracking-af847.firebaseapp.com",
  databaseURL: "https://user-tracking-af847.firebaseio.com",
  storageBucket: "user-tracking-af847.appspot.com",
  messagingSenderId: "840819858389"
};

// Configuring Firebase auth
const myFirebaseAuthConfig = {
  provider: AuthProviders.Password,
  method: AuthMethods.Password
}

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    ModalPage,
    ResetPasswordPage,
    SignUpPage,
    MapPage,
    ListPage,
    AutocompletePage,
    AutocompletedropoffPage,
    ModalTestPagePage,
    TermsAndConditionPage,
    AboutPage,
    PaymentPage,
    PreorderRidePage,
    ProfilePage,
    EditProfilePage,
    RidePage,
    ScheduledRidesPage,
    RideHistoryPage,
    IncentiveProgramsPage
  ],
  imports: [
    IonicModule.forRoot(MyApp), IonicImageLoader,
    AgmCoreModule.forRoot({ apiKey: 'AIzaSyCFAL58sFPY1c99GA-AuEhjDxIMDrKT_kY', libraries: ["places"]}),
    AngularFireModule.initializeApp(firebaseConfig, myFirebaseAuthConfig),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    ModalPage,
    ResetPasswordPage,
    SignUpPage,
    MapPage,
    ListPage,
    AutocompletePage,
    AutocompletedropoffPage,
    ModalTestPagePage,
    TermsAndConditionPage,
    AboutPage,
    PaymentPage,
    PreorderRidePage,
    ProfilePage,
    EditProfilePage,
    RidePage,
    ScheduledRidesPage,
    RideHistoryPage,
    IncentiveProgramsPage
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler}, AuthData, FcmNotification, Connectivity, GoogleMaps]
})
export class AppModule {}
