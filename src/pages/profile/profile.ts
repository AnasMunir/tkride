import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { EditProfilePage } from '../edit-profile/edit-profile';

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html'
})
export class ProfilePage {

  name: any; firstName: any; lastName: any; email: any; cc: any; cvv: any; month: any; year: any;
  constructor(public navCtrl: NavController) {}

  ionViewDidLoad() {
    console.log('Hello ProfilePage Page');
    let user = firebase.auth().currentUser;
    let ref = firebase.database().ref('users/'+user.uid);
    ref.on("value", (snap) => {
      console.log(snap.val());
      this.firstName = snap.val().firstName;
      this.lastName = snap.val().lastName;
      this.email = snap.val().email;
      this.cc = snap.val().cc;
      this.cvv = snap.val().cvv;
      this.month = snap.val().month;
      this.year = snap.val().year;
      this.name = this.firstName + ' ' + this.lastName;
    });
  }
  editProfile() {
    this.navCtrl.push(EditProfilePage,
      {firstName: this.firstName,
        lastName: this.lastName,
        cc: this.cc,
        cvv: this.cvv,
        month: this.month,
        year: this.year,
      });
  }

}
