import { Component } from '@angular/core';
import { NavController} from 'ionic-angular';
import { LoginPage } from '../login/login';

import { MapPage } from '../map/map';
import { ListPage } from '../list/list';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  tab1Root: any = MapPage;
  tab2Root: any = ListPage;

  ionViewDidLoad() {
    console.log('Hello User HomePage');
  }
  constructor(public navCtrl: NavController) { }
  logoutUser() {
    this.navCtrl.setRoot(LoginPage);
  }
}
