import { Component, OnInit } from '@angular/core';
import { ViewController } from 'ionic-angular';

declare var google: any;

@Component({
  selector: 'page-autocompletedropoff',
  templateUrl: 'autocompletedropoff.html'
})
export class AutocompletedropoffPage implements OnInit{

  autocompleteItems: any;
  autocomplete: any;
  acService:any;
  placesService: any;

  constructor(public viewCtrl: ViewController) {}

  ionViewDidLoad() {
    console.log('Hello AutocompletedropoffPage Page');
  }

  ngOnInit() {
    this.acService = new google.maps.places.AutocompleteService();
    this.autocompleteItems = [];
    this.autocomplete = {
      query: ''
    };
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  DropoffChooseItem(item: any) {
    console.log('modal > chooseItem > item > ', item);
    
    this.viewCtrl.dismiss(item);
  }

  updateSearch() {
    console.log('modal > updateSearch');
    if (this.autocomplete.query == '') {
      this.autocompleteItems = [];
      return;
    }
    let self = this;
    let config = {
      types:  ['geocode'], // other types available in the API: 'establishment', 'regions', and 'cities'
      input: this.autocomplete.query,
      componentRestrictions: { country: 'PK' }
    }
    this.acService.getPlacePredictions(config, function (predictions, status) {
      console.log('modal > getPlacePredictions > status > ', status);
      self.autocompleteItems = [];
      predictions.forEach(function (prediction) {
        self.autocompleteItems.push(prediction);
      });
    });
  }
}
