import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import {AngularFire, /*FirebaseListObservable,*/ FirebaseObjectObservable} from 'angularfire2';

@Component({
  selector: 'page-scheduled-rides',
  templateUrl: 'scheduled-rides.html'
})
export class ScheduledRidesPage {

  objects: FirebaseObjectObservable<any>;
  rides: any

  constructor(public navCtrl: NavController, public af: AngularFire) {

    let user = firebase.auth().currentUser;
    this.objects = af.database.object('users/' + user.uid, { preserveSnapshot: true });
    this.objects.first().subscribe(snap => {
      console.log('from objects');
      console.log(snap.val().scheduledRides);
      this.rides = snap.val().scheduledRides;
    })
  }

  ionViewDidLoad() {
    console.log('Hello ScheduledRidesPage Page');
  }

}
