import { Component } from '@angular/core';
import { NavController, Platform, ViewController, NavParams, LoadingController, AlertController } from 'ionic-angular';


import { HomePage } from '../home/home';
import { RidePage } from '../ride/ride';
import { ListPage } from '../list/list';

import { FcmNotification } from '../../providers/fcm-notification';

declare var FCMPlugin: any;

@Component({
  selector: 'page-modal-page',
  templateUrl: 'modal-page.html'
})
export class ModalPage {
  driver;
  name;
  car_color;
  car_make;
  car_model;
  from; to; lat; lng;
  public pickupLat; public pickupLng;

  constructor(
    public navCtrl: NavController,
    public platform: Platform,
    public viewCtrl: ViewController,
    public navParams: NavParams, public loadingCtrl: LoadingController,
    public fcmNotification: FcmNotification, public alertCtrl: AlertController
  ) {
    FCMPlugin.onNotification(
      (data) => {
        if (data.wasTapped) {
          console.log(data);
          this.pickupLat = JSON.parse(data.lat);
          this.pickupLng = JSON.parse(data.lng);
          let driverId = data.driverId;
          console.log(data.lat);
          console.log(data.lng);
          // confirm.setSubTitle('Response: ' + data.response);
          if (JSON.parse(data.response) === true) {
            let data = {'pickupLat': this.pickupLat, 'pickupLng': this.pickupLng, 'driverId': driverId};
            this.viewCtrl.dismiss(data);
            // this.navCtrl.setRoot(RidePage, {pickupLat: this.pickupLat, pickupLng: this.pickupLng, driverId: driverId});
          } else {
            let alert = this.alertCtrl.create({ title: 'Driver rejected your ride' })
            alert.present();
            this.viewCtrl.dismiss();
          }
        } else {
          console.log(data);
          this.pickupLat = JSON.parse(data.lat);
          this.pickupLng = JSON.parse(data.lng);
          let driverId = data.driverId;
          console.log(data.lat);
          console.log(data.lng);
          if (JSON.parse(data.response) === true) {
            let data = {'pickupLat': this.pickupLat, 'pickupLng': this.pickupLng, 'driverId': driverId};
            this.viewCtrl.dismiss(data);
            // this.navCtrl.setRoot(RidePage, {pickupLat: this.pickupLat, pickupLng: this.pickupLng, driverId: driverId});
          } else {
            let alert = this.alertCtrl.create({ title: 'Driver rejected your ride' })
            alert.present();
            this.viewCtrl.dismiss();
          }
        }
      },
      (msg) => {
        console.log('onNotification callback successfully registered: ' + msg);
      },
      (err) => {
        console.log('Error registering onNotification callback: ' + err);
      }
    );

    this.driver = this.navParams.get('driver');

    this.from = this.navParams.get('from');
    this.to = this.navParams.get('to');
    this.lat = this.navParams.get('lat');
    this.lng = this.navParams.get('lng');
    console.log('from ' + this.from); console.log('to ' + this.to);
    console.log('lat: ' + this.lat); console.log('lng ' + this.lng);

    // console.log(this.name.fullname);
    console.log(this.driver);
    console.log(this.driver.car_make);
    console.log(this.driver.car_model);
    console.log(this.driver.car_color);
  }

  ionViewDidLoad() {
    console.log('Hello ModalPagePage Page');
  }
  dismiss() {
    this.viewCtrl.dismiss();
  }
  sendNotificaiton() {
    let user = firebase.auth().currentUser;
    let ref = firebase.database().ref('users/' + user.uid);
    // let clientToken = ref.child('fcm_token');
    // let clientName = ref.child('name');
    let loader = this.loadingCtrl.create({
      content: "Waiting for reply Please wait...",
      dismissOnPageChange: true,
      duration: 20000
    });
    let driver_token = this.driver.fcm_token;

    ref.once("value", (snap) => {
      let clientToken = snap.val().fcm_token;
      let clientName = snap.val().name
      console.log('client token: ' + clientToken);
      this.fcmNotification.sendNotificaiton(driver_token, clientToken, clientName, this.from, this.to, this.lat, this.lng, user.uid)
      loader.present();
    });


    console.log('driver token from modal:');
    console.log('driver_token ' + driver_token);


    // console.log('driver: ' + driver_token );
    // console.log('client: ' + client_token);
  }

}
