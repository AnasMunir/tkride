import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ViewController } from 'ionic-angular';

declare var google: any;

@Component({
  selector: 'page-autocomplete',
  templateUrl: 'autocomplete.html'
})
export class AutocompletePage implements OnInit{

  @ViewChild('searchbar') searchbarElement: ElementRef;

  ionViewDidLoad() {
    console.log('Hello AutocompletePage Page');
  }

  autocompleteItems: any;
  autocomplete: any;
  acService:any;
  placesService: any;
  // service = new google.maps.places.AutocompleteService();

  constructor (public viewCtrl: ViewController) { }

  ngOnInit() {
    this.acService = new google.maps.places.AutocompleteService();
    this.autocompleteItems = [];
    this.autocomplete = {
      query: ''
    };
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  PickupChooseItem(item: any) {
    console.log('modal > chooseItem > item > ', item);

    // let activeElement = this.searchbarElement.nativeElement;
    // activeElement.blur();



    this.viewCtrl.dismiss(item);
  }

  updateSearch() {
    console.log('modal > updateSearch');
    if (this.autocomplete.query == '') {
      this.autocompleteItems = [];
      return;
    }
    let self = this;
    let config = {
      types:  ['geocode'], // other types available in the API: 'establishment', 'regions', and 'cities'
      input: this.autocomplete.query,
      componentRestrictions: { country: 'PK' }
    }
    this.acService.getPlacePredictions(config, function (predictions, status) {
      console.log('modal > getPlacePredictions > status > ', status);
      self.autocompleteItems = [];
      predictions.forEach(function (prediction) {
        self.autocompleteItems.push(prediction);
      });
    });
  }
}
