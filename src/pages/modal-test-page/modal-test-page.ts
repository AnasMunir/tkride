import { Component } from '@angular/core';
import { ViewController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { FcmNotification } from '../../providers/fcm-notification';

@Component({
  selector: 'page-modal-test-page',
  templateUrl: 'modal-test-page.html'
})
export class ModalTestPagePage {

  constructor(public viewCtrl: ViewController) {}

  ionViewDidLoad() {
    console.log('Hello ModalTestPagePage Page');
  }
  dismiss() {
    this.viewCtrl.dismiss();
  }

}
