import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'page-edit-profile',
  templateUrl: 'edit-profile.html'
})
export class EditProfilePage {
  submitAttempt: boolean = false;
  profileForm: any;
  firstName: any; lastName: any; cc: any; cvv: any; month: any; year: any;
  loading: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public formBilder: FormBuilder,
    public loadingCtrl: LoadingController) {

    this.firstName = this.navParams.get('firstName');
    this.lastName = this.navParams.get('lastName');
    this.cc = this.navParams.get('cc');
    this.cvv = this.navParams.get('cvv');
    this.month = this.navParams.get('month');
    this.year = this.navParams.get('year');

    this.profileForm = formBilder.group({
      firstName: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*')])],
      lastName: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*')])],
      cc: ['', Validators.compose([Validators.minLength(16), Validators.maxLength(16), Validators.pattern('[0-9]*')])],
      cvv: ['', Validators.compose([Validators.minLength(3), Validators.maxLength(3), Validators.pattern('[0-9]*')])],
      month: ['', Validators.compose([Validators.minLength(2), Validators.maxLength(2)])],
      year: ['', Validators.compose([Validators.minLength(2), Validators.maxLength(2)])]
    })
  }

  ionViewDidLoad() {
    console.log('Hello EditProfilePage Page');
  }
  updateProfile() {
    this.submitAttempt = true;
    if(!this.profileForm.valid) {
      console.log(' Some values were not given or were incorrect, please fill them');
    } else {
      console.log('success!');
      console.log(this.profileForm.value);
      let user = firebase.auth().currentUser;
      let ref = firebase.database().ref('users/' + user.uid);
      if(this.profileForm.value.firstName) {
        ref.update({firstName: this.profileForm.value.firstName})
      }
      if(this.profileForm.value.lastName) {
        ref.update({lastName: this.profileForm.value.lastName})
      }
      if(this.profileForm.value.cc) {
        ref.update({cc: this.profileForm.value.cc})
      }
      if(this.profileForm.value.cvv) {
        ref.update({cvv: this.profileForm.value.cvv})
      }
      if(this.profileForm.value.month) {
        ref.update({month: this.profileForm.value.month})
      }
      if(this.profileForm.value.year) {
        ref.update({year: this.profileForm.value.year})
      }
      // ref.update({
      //   lastName: this.profileForm.value.lastName,
      //   name: this.profileForm.value.firstName + ' ' + this.profileForm.value.lastName,
      //   cc: this.profileForm.value.cc,
      //   cvv: this.profileForm.value.cvv,
      //   month: this.profileForm.value.month,
      //   year: this.profileForm.value.year
      // }).then(() => {
      //   this.navCtrl.pop();
      // }).catch(err => {console.log(err)});
      //
      // this.loading = this.loadingCtrl.create({
      //   dismissOnPageChange: true,
      // });
      // this.loading.present();
      this.navCtrl.pop();
      this.loading = this.loadingCtrl.create({
        dismissOnPageChange: true,
      });
      this.loading.present();
    }
  }

}
