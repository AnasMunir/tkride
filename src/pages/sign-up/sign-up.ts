import { Component, ViewChild } from '@angular/core';
import { NavController, LoadingController, AlertController, ModalController, MenuController } from 'ionic-angular';
import { FormBuilder, Validators, FormControl, FormControlName, NgControl } from '@angular/forms';
import { Camera } from 'ionic-native';
import { AuthData } from '../../providers/auth-data';
import { EmailValidator } from '../../validators/email';

import { TermsAndConditionPage } from '../terms-and-condition/terms-and-condition';
import { PaymentPage } from '../payment/payment';
// import { HomePage } from '../home/home';

@Component({
  selector: 'page-sign-up',
  templateUrl: 'sign-up.html'
})
export class SignUpPage {

  public signupForm;
  public disabled = true;

  public checkbox = {checked: true, unchecked: false}
  // fullNameChanged: boolean = false;
  firstNameChanged: boolean = false;
  lastNameChanged: boolean = false;
  emailChanged: boolean = false;
  phoneNumberChanged: boolean = false;
  passwordChanged: boolean = false;

  submitAttempt: boolean = false;
  loading;


  constructor(public navCtrl: NavController, public authData: AuthData,
    public formBuilder: FormBuilder,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public menuCtrl: MenuController) {

      this.menuCtrl.enable(false);
      this.signupForm = formBuilder.group({
        // fullName: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
        firstName: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
        lastName: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
        email: ['', Validators.compose([Validators.required, EmailValidator.isValid])],
        password: ['', Validators.compose([Validators.minLength(6), Validators.required])]
      });
    }
    /**
    * Receives an input field and sets the corresponding fieldChanged property to 'true' to help with the styles.
    */
    elementChanged(input){
      let field = input.inputControl.name;
      this[field + "Changed"] = true;
    }

    /**
    * If the form is valid it will call the AuthData service to sign the user up password displaying a loading
    *  component while the user waits.
    *
    * If the form is invalid it will just log the form value, feel free to handle that as you like.
    */
    signupUser(){
      this.submitAttempt = true;

      if (!this.signupForm.valid){
        // this.signupSlider.slideTo(0);
        console.log(this.signupForm.value);
      } else {
        this.navCtrl.push(PaymentPage, {signupFormData: this.signupForm.value});
        // this.authData.signUpUser(
        //   this.signupForm.value.email,
        //   this.signupForm.value.password,
        //   this.signupForm.value.firstName,
        //   this.signupForm.value.lastName
        //   // this.signupForm.value.fullName
        // ).then(() => {
        // }, (error) => {
        //   this.loading.dismiss().then( () => {
        //     var errorMessage: string = error.message;
        //     let alert = this.alertCtrl.create({
        //       message: errorMessage,
        //       buttons: [
        //         {
        //           text: "Ok",
        //           role: 'cancel'
        //         }
        //       ]
        //     });
        //     alert.present();
        //   });
        // });
        //
        // this.loading = this.loadingCtrl.create({
        //   dismissOnPageChange: true,
        // });
        // this.loading.present();
      }
    }
    enable(e) {
      console.log(e);
      if(e.checked === true) {
          this.disabled = false;
      }
      else {
        this.disabled = true;
      }
    }
    termsAndConditions() {
      let modal = this.modalCtrl.create(TermsAndConditionPage);
      // modal.onDidDismiss((data) => {
      //   console.log('Dismiss request modal');
      //   console.log(data);
      //
      //   this.navCtrl.setRoot(RidePage, {pickupLat: data.pickupLat, pickupLng: data.pickupLng, driverId: data.driverId});
      // })
      modal.present();
    }

  ionViewDidLoad() {

    console.log('Hello SignUpPage Page');
  }

}
