import { Component } from '@angular/core';
import { NavController, ViewController } from 'ionic-angular';

/*
  Generated class for the TermsAndCondition page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-terms-and-condition',
  templateUrl: 'terms-and-condition.html'
})
export class TermsAndConditionPage {

  constructor(public navCtrl: NavController,public viewCtrl: ViewController) {
  }
  dismiss() {
    this.viewCtrl.dismiss();
  }

  ionViewDidLoad() {
    console.log('Hello TermsAndConditionPage Page');
  }

}
