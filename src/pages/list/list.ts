import { Component } from '@angular/core';
import { MenuController, NavController, ModalController, NavParams } from 'ionic-angular';
import { Geolocation, Geoposition } from 'ionic-native';
import { AngularFire, FirebaseObjectObservable, FirebaseListObservable } from 'angularfire2';

import { LoginPage } from '../login/login';
import { ModalPage } from '../modal-page/modal-page';
import { RidePage } from '../ride/ride';

// import { ModalTestPagePage } from '../modal-test-page/modal-test-page';

import 'rxjs/Rx';
import { Observable } from 'rxjs/Rx';

// declare var FCMPlugin: any;
declare var GeoFire: any;

@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {


  drivers: any[] = [];
  from; to;
  public lat: number = 0;
  public lng: number = 0;

  public latG: number = 0;
  public lngG: number = 0;

  drivers$: FirebaseObjectObservable<any>;

  ionViewDidLoad() {
    console.log('Hello User ListPage');
  }
  constructor(public navCtrl: NavController, public menu: MenuController,
    public af: AngularFire,
    public modalCtrl: ModalController, public navParams: NavParams) {

      menu.enable(true);
      this.from = navParams.get('from');
      this.to = navParams.get('to');
      this.lat = navParams.get('lat');
      this.lng = navParams.get('lng');
      console.log('from '+ this.from); console.log('to ' + this.to);
      console.log('lat ' + this.lat); console.log('lng ' + this.lng);
      // setInterval(this.setLocations, 5000);
      this.getNearbyDrivers();
      let user = firebase.auth().currentUser;
      let ref = firebase.database().ref('users/'+user.uid);

    //   FCMPlugin.getToken(
    //     (token) => {
    //       // alert(token);
    //       console.log(token);
    //       ref.update({fcm_token: token});
    //     },
    //     (err) => {
    //       console.log('error retrieving token: ' + err);
    //     }
    //   );
    //   FCMPlugin.onNotification( (data) => {
    //     if(data.wasTapped) {
    //       console.log(data)
    //     } else {
    //       console.log(data);
    //     }
    //   }, (msg) => {
    //     console.log('onNotification callback successfully registered: ' + msg);
    //   }, (err) => {
    //     console.log('Error registering onNotification callback: ' + err);
    //   }
    // );
  }
  setLocations() {
    let user = firebase.auth().currentUser;
    let ref = firebase.database().ref('users/locations');

    var geoFire = new GeoFire(ref);
    let options = {
        frequency: 3000,
        enableHighAccuracy: true
      };
    Geolocation.getCurrentPosition(options).then((pos: Geoposition) => {
      geoFire.set(user.uid, [pos.coords.latitude, pos.coords.longitude]).then(() => {
        ref.child(user.uid).onDisconnect().remove();
      }).catch(err => {console.log(err)});
    });
  }
  getNearbyDrivers() {
    let ref = firebase.database().ref('drivers/locations');
    let user = firebase.auth().currentUser;
    this.drivers$ = this.af.database.object('drivers/');
    var geoFire = new GeoFire(ref);
    let options = {
      frequency: 3000,
      enableHighAccuracy: true
    };
    Geolocation.getCurrentPosition(options).then((pos: Geoposition) => {
      this.latG = pos.coords.latitude; this.lngG = pos.coords.longitude;
      console.log('lat ' + this.latG + ' lng ' + this.lngG);
      var geoQuery = geoFire.query({
        center: [this.latG, this.lngG],
        radius: 5
      });

      geoQuery.on("key_entered", key => {
        this.drivers$.first().subscribe(snapshot => {
          this.drivers.push(snapshot[key]);
          console.log(this.drivers);
        });
      });
      geoQuery.on("key_exited", key => {
        this.drivers$.first().subscribe(snapshot => {
          console.log(this.drivers);

          let index = this.drivers.findIndex(obj => {
            return obj.fullname === snapshot[key]['fullname'];
          });
          this.drivers.splice(index, 1);
          console.log('THe snapshot');
          console.log(snapshot[key]);
          console.log("It's index: " + index);
          console.log('new array');
          console.log(this.drivers);
        })
      })
    });

  }
  logoutUser() {
    this.navCtrl.setRoot(LoginPage);
  }
  openModal(driver) {
    console.log('driver: ' + driver);
    console.log('from '+ this.from); console.log('to ' + this.to);
    let modal = this.modalCtrl.create(ModalPage, {driver, from: this.from, to: this.to, lat: this.lat, lng: this.lng});
    modal.onDidDismiss((data) => {
      console.log('Dismiss request modal');
      console.log(data);
      if(data) {
        this.navCtrl.setRoot(RidePage, {pickupLat: data.pickupLat, pickupLng: data.pickupLng, driverId: data.driverId});
      }
    })
    modal.present();
  }
  // openTestModal() {
  //   let modal = this.modalCtrl.create(ModalTestPagePage);
  //   modal.present();
  // }
}
