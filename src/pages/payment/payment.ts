import { Component } from '@angular/core';
import { NavController, AlertController, LoadingController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup ,Validators, FormControl, FormControlName, NgControl } from '@angular/forms';
import { AuthData } from '../../providers/auth-data';
// import { HomePage } from '../home/home';
import { MapPage } from '../map/map';

declare var Stripe: any;

@Component({
  selector: 'page-payment',
  templateUrl: 'payment.html'
})
export class PaymentPage {

  public paymentForm;
  public signupData: any;
  loading;
  minYear: any; maxYear: any;
  submitAttempt: boolean = false;
  private token: string = '';
  // private ngForm: any = {
  //     cc: '',
  //     cvc: '',
  //     month: '',
  //     year: '',
  //     // amount: 2000
  // };
  constructor(public navCtrl: NavController,
    public formBuilder: FormBuilder,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public navParams: NavParams,
    public authData: AuthData) {


    this.signupData = this.navParams.get('signupFormData');
    console.log(this.signupData);
    console.log(Stripe);
    Stripe.setPublishableKey('pk_test_UR9taDhVZk9fPqK1UppYScbm');

    let date = new Date();
    this.minYear = date.getFullYear();
    this.maxYear = this.minYear + 5;

    this.paymentForm = formBuilder.group({
        cc: ['', Validators.compose([Validators.minLength(16), Validators.maxLength(16), Validators.pattern('[0-9]*'), Validators.required])],
        cvv: ['', Validators.compose([Validators.minLength(3), Validators.maxLength(3), Validators.pattern('[0-9]*'), Validators.required])],
        month: ['', Validators.compose([Validators.minLength(2), Validators.maxLength(2), Validators.required])],
        year: ['', Validators.compose([Validators.minLength(2), Validators.maxLength(2), Validators.required])],
      });
  }

  submit() {

    this.submitAttempt = true;
    if(!this.paymentForm.value){
      console.log(' Some values were not right')
    } else {
      console.log('success');
      console.log('Payment form ');
      console.log(this.paymentForm.value);
      // let user = firebase.auth().currentUser;
      // console.log('user uid: ' + user.uid);
      // let ref = firebase.database().ref('users/' + user.uid);
      // ref.update({
      //   cc: this.paymentForm.value.cc,
      //   cvv: this.paymentForm.value.cvv,
      //   month: this.paymentForm.value.month,
      //   year: this.paymentForm.value.year
      // });
      this.loading = this.loadingCtrl.create({
        dismissOnPageChange: true,
      });
      this.loading.present();
      
      Stripe.card.createToken({
        number: this.paymentForm.value.cc, //'4242424242424242',
        cvc: this.paymentForm.value.cvv, //'123',
        exp_month: this.paymentForm.value.month, //12,
        exp_year: this.paymentForm.value.year, //2017,
        // amount: 2000
      }, (status, response) => this.stripeResponseHandler(status, response));
    }
    // this.navCtrl.setRoot(HomePage);
  }

  stripeResponseHandler(status, response) {

    if (response.error) {
      // Show the errors on the form
      console.log('error');
      console.log(response.error.message);
      let prompt = this.alertCtrl.create({
        title: 'Your credit card information was rejected!',
      message: response.error.message,
      buttons: [
        {
          text: 'Try Again',
          handler: () => {
            console.log('try again clicked');
          }
        }]
      });
      prompt.present();
    } else {
      // response contains id and card, which contains additional card details
      this.token = response.id;
      // Insert the token into the form so it gets submitted to the server
      console.log('success');
      console.log('Sending token param:');
      console.log(this.token);
      this.authData.signUpUser(
        this.signupData.email,
        this.signupData.password,
        this.signupData.firstName,
        this.signupData.lastName,
        this.paymentForm.value.cc,
        this.paymentForm.value.cvv,
        this.paymentForm.value.month,
        this.paymentForm.value.year,
        this.token
        // this.signupForm.value.fullName
      ).then(() => {
        let alert = this.alertCtrl.create({
          message: 'Welcome, you have successfully registered with TKRIDE',
          buttons: [
            {
              text: "Ok"
            }
          ]
        });
        alert.present();
        this.navCtrl.setRoot(MapPage);
      }, (error) => {
        this.loading.dismiss().then( () => {
          var errorMessage: string = error.message;
          let alert = this.alertCtrl.create({
            message: errorMessage,
            buttons: [
              {
                text: "Ok",
                role: 'cancel'
              }
            ]
          });
          alert.present();
        });
      });


      // this.navCtrl.setRoot(MapPage);
      // this.nav.push(ThankyouPage, {token: this.token});
    }
  }

  ionViewDidLoad() {
    console.log('Hello PaymentPage Page');
  }

}
