import { Component, ElementRef, ViewChild } from '@angular/core';
import { NavController, MenuController, AlertController, LoadingController } from 'ionic-angular';
import { Geolocation, Geoposition, BackgroundGeolocation } from 'ionic-native';
import { AngularFire, FirebaseObjectObservable } from 'angularfire2';
import { NgControl, FormControlName } from '@angular/forms';

import { ScheduledRidesPage } from '../scheduled-rides/scheduled-rides';
declare var google: any;


@Component({
  selector: 'page-preorder-ride',
  templateUrl: 'preorder-ride.html'
})
export class PreorderRidePage {
  @ViewChild('map') mapElement: ElementRef;

  drivers: any[] = [];
  driverKeys: any[] = [];
  map: any;
  address_pickup;
  address_dropoff;
  public lat: number = 0;
  public lng: number = 0;

  scheduledRideDate: any;
  saveDisabled: boolean = true;
  placesService:any;
  distanceService:any;
  pickUpLat: number; pickUpLng: number;
  dropOffLat: number; dropOffLng: number;
  distance:any; duration:any;  estimate:number;

  users$: FirebaseObjectObservable<any>;
  public dateForm: any;
  // driversList$: FirebaseListObservable<any>;

  autocompleteItemsPickup: any;
  autocompleteItemsDropoff: any;
  autocompletePickup: any;
  autocompleteDropoff: any;
  acService:any;
  shouldHidePickup: boolean;
  shouldHideDropoff: boolean;
  disabled: boolean;
  pickupPlaceholder = "Enter pickup location ...";
  dropoffPlaceholder = "Enter dropoff location ...";

  constructor(public navCtrl: NavController,
    public menu: MenuController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public af: AngularFire) {

    menu.enable(true);
    let self = this;
    Geolocation.getCurrentPosition().then((position) => {
      // UNCOMMENT FOR NORMAL USE
      let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
      this.pickUpLat = position.coords.latitude;
      this.pickUpLng = position.coords.longitude;
      this.geocodeLatLng(position.coords.latitude, position.coords.longitude);
      // let latLng = new google.maps.LatLng(40.713744, -74.009056);


      let mapOptions = {
        center: latLng,
        scroll: true,
        rotate: true,
        zoomControl: false,
        disableDefaultUI: true,
        mapTypeControl: false,
        zoom: 15,
        tilt: 30,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }

      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

      google.maps.event.addListener(this.map,'idle',function(){
        if(!this.get('dragging') && this.get('oldCenter') && this.get('oldCenter')!==this.getCenter()) {
          //do what you want to
          // myMarker.setPosition(this.getCenter());
          let maanBhootni = this.getCenter();
          self.pickUpLat = maanBhootni.lat();
          self.pickUpLng = maanBhootni.lng();
          console.log(maanBhootni.lat());
          console.log(maanBhootni.lng());
          self.geocodeLatLng(maanBhootni.lat(), maanBhootni.lng());
          console.log(this.getCenter());
        }
        if(!this.get('dragging')){
          console.log(this.getCenter());
          this.set('oldCenter',this.getCenter());
        }

      });
      google.maps.event.addListener(this.map,'dragstart',function(){
        this.set('dragging',true);
      });

      google.maps.event.addListener(this.map,'dragend',function(){
        this.set('dragging',false);
        google.maps.event.trigger(this,'idle',{});
      });
    });

    let user = firebase.auth().currentUser;
    let ref = firebase.database().ref('users/'+user.uid);
    this.address_pickup = {
      place: '',
      set: false
    };
    this.address_dropoff = {
      place: '',
      set: false
    };
    let confirm = this.alertCtrl.create({
      title: "Client Request"
    });
  }

  ionViewDidLoad() {
    console.log('Hello PreorderRidePage Page');
  }
  ngOnInit() {
    let user = firebase.auth().currentUser;
    let ref = firebase.database().ref('users/'+user.uid);

    this.acService = new google.maps.places.AutocompleteService();
    this.autocompleteItemsPickup = [];
    this.autocompleteItemsDropoff = [];
    this.autocompletePickup = {
      query: ''
    };
    this.autocompleteDropoff = {
      query: ''
    };
    this.shouldHidePickup = false;
    this.shouldHideDropoff = false;
    this.disabled = false;
  }

  adjustLocation() {
    Geolocation.getCurrentPosition().then((position) => {
      let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
      // this.map.setCenter(latLng);
      this.map.panTo(latLng);
    })
  }
  geocodeLatLng(lat, lng) {
    let geocoder = new google.maps.Geocoder;
    let latLng = new google.maps.LatLng(lat, lng);
    console.log(latLng);
    let self = this;
    geocoder.geocode({'location': latLng}, function(results, status) {
    if (status === 'OK') {
      if (results[1]) {
        console.log(results);
        // self.getPickupPlaceDetail(results[1]['place_id']);
        console.log(results[0]['address_components'][0].long_name);
        console.log(results[1]['address_components'][0].long_name);
        console.log(results[0]['address_components'][1].long_name);
        let pickupLocation = results[0]['address_components'][0].long_name + ' ' + results[1]['address_components'][0].long_name + ' ' + results[0]['address_components'][1].long_name;
        self.pickupPlaceholder = pickupLocation;
        self.address_pickup.place = pickupLocation;
      } else {
        console.log('No results found');
      }
    } else {
      console.log('Geocoder failed due to: ' + status);
    }
  });
  }

  PickupChooseItem(item: any) {
    console.log('modal > chooseItem > item > ', item);

    // this.viewCtrl.dismiss(item);
    this.shouldHidePickup = true;

    this.address_pickup.place = item.description;
    // this.address_pickup.set = true;
    console.log('The data');
    console.log(item);
    // this.pickupSearch.nativeElement.placeholder = this.address_pickup.place
    this.pickupPlaceholder = this.address_pickup.place

    if(item){
      // get details
      console.log('pick up details');
      this.getPickupPlaceDetail(item.place_id);

      // this.getPlaceDetail(data.place_id);
    }
  }
  DropoffChooseItem(item: any, button?) {
    console.log('modal > chooseItem > item > ', item);
    this.shouldHideDropoff = true;
    this.disabled = true;
    this.address_pickup.set = true;
    this.address_dropoff.place = item.description;
    this.address_dropoff.set = true;
    console.log('The data');
    console.log(item);
    this.dropoffPlaceholder = this.address_dropoff.place;
    if(item){
      // get details
      console.log('drop off details');
      this.getDropoffPlaceDetail(item.place_id);

      // this.getPlaceDetail(data.place_id);
    }

  }

  updateSearchPickup() {
    console.log('modal > updateSearch');
    if (this.autocompletePickup.query == '') {
      this.autocompleteItemsPickup = [];
      return;
    }
    let self = this;
    let config = {
      types:  ['geocode'], // other types available in the API: 'establishment', 'regions', and 'cities'
      input: this.autocompletePickup.query,
      componentRestrictions: { country: 'PK' }
    }
    this.acService.getPlacePredictions(config, function (predictions, status) {
      console.log('modal > getPlacePredictions > status > ', status);
      self.autocompleteItemsPickup = [];
      predictions.forEach(function (prediction) {
        self.autocompleteItemsPickup.push(prediction);
      });
    });
  }
  updateSearchDropoff() {
    console.log('modal > updateSearch');
    if (this.autocompleteDropoff.query == '') {
      this.autocompleteItemsDropoff = [];
      return;
    }
    let self = this;
    let config = {
      types:  ['geocode'], // other types available in the API: 'establishment', 'regions', and 'cities'
      input: this.autocompleteDropoff.query,
      componentRestrictions: { country: 'PK' }
    }
    this.acService.getPlacePredictions(config, function (predictions, status) {
      console.log('modal > getPlacePredictions > status > ', status);
      self.autocompleteItemsDropoff = [];
      predictions.forEach(function (prediction) {
        self.autocompleteItemsDropoff.push(prediction);
      });
    });
  }

  getPickupPlaceDetail(place_id:string) {
    var self = this;
    var request = {
      placeId: place_id
    };
    this.placesService = new google.maps.places.PlacesService(this.mapElement.nativeElement);
    this.placesService.getDetails(request, callback);
    function callback(place, status) {
      if (status === google.maps.places.PlacesServiceStatus.OK) {

        console.log('page > getPlaceDetail > place > ');
        console.log(place);
        console.log(place.geometry.location.lat());
        self.pickUpLat = place.geometry.location.lat();
        self.pickUpLng = place.geometry.location.lng();
        console.log(self.pickUpLat); console.log(self.pickUpLng);

        // self.getPlaceDistance()
      }
      // self.map = self.maps.init(self.mapElement.nativeElement, self.pleaseConnect.nativeElement);
      // self.map.setCenter(place.geometry.location);
    }
  }
  getDropoffPlaceDetail(place_id:string) {
    var self = this;
    var request = {
      placeId: place_id
    };
    this.placesService = new google.maps.places.PlacesService(this.mapElement.nativeElement);
    this.placesService.getDetails(request, callback);
    function callback(place, status) {
      if (status === google.maps.places.PlacesServiceStatus.OK) {

        console.log('page > getPlaceDetail > place > ');
        console.log(place);
        console.log(place.geometry.location.lat());
        self.dropOffLat = place.geometry.location.lat();
        self.dropOffLng = place.geometry.location.lng();
        console.log(self.dropOffLat); console.log(self.dropOffLng);
        self.getPlaceDistance(self.pickUpLat, self.pickUpLng, self.dropOffLat, self.dropOffLng);

        // self.getPlaceDistance()
      }
      // self.map.setCenter(place.geometry.location);
      // self.map = self.maps.init(self.mapElement.nativeElement);
      self.mapInit();
    }
  }
  getPlaceDistance(originLat, originLng, destinationLat, destinationLng) {
    var self = this;
    this.distanceService = new google.maps.DistanceMatrixService();
    let origin = new google.maps.LatLng(originLat, originLng);
    let destination = new google.maps.LatLng(destinationLat, destinationLng);
    this.distanceService.getDistanceMatrix(
      {
        origins: [origin],
        destinations: [destination],
        travelMode: 'DRIVING',
        // transitOptions: TransitOptions,
        // drivingOptions: DrivingOptions,
        unitSystem: google.maps.UnitSystem.IMPERIAL,
        avoidHighways: false,
        avoidTolls: false,
      }, callback);
      function callback(response, status) {
        console.log('page > getPlaceDistance > Response > ');
        console.log(response);
        console.log(response.rows[0].elements[0].distance.text);
        console.log(response.rows[0].elements[0].duration.text);
        self.distance = response.rows[0].elements[0].distance.text;
        self.duration = response.rows[0].elements[0].duration.text;
        self.estimate = 7.5 + (0.00170878 * response.rows[0].elements[0].distance.value) + (0.0083333 * response.rows[0].elements[0].duration.value);

        let distance = 0.0016777 * response.rows[0].elements[0].distance.value;
        let duration = 0.0058333 * response.rows[0].elements[0].duration.value;


        console.log(distance + duration + 7.5);
        console.log('estimate '+ self.estimate);

        // if(status === google.maps.DistanceMatrixService.OK) {
        // }
        // See Parsing the Results for
        // the basics of a callback function.
      }
  }

  mapInit() {
    Geolocation.getCurrentPosition().then((position) => {
      // UNCOMMENT FOR NORMAL USE
      let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

      // let latLng = new google.maps.LatLng(40.713744, -74.009056);


      let mapOptions = {
        center: latLng,
        scroll: true,
        rotate: true,
        zoomControl: false,
        disableDefaultUI: true,
        mapTypeControl: false,
        zoom: 15,
        tilt: 30,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }

      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

    });
  }

  bookRide() {
    console.log('booking ride');
    console.log(this.scheduledRideDate);
    let save = this.alertCtrl.create({
      title: "Your ride has been scheduled",
      buttons: [
        {
          text: 'OK',
          handler: data => {
            this.navCtrl.setRoot(ScheduledRidesPage);
          }
        }
      ]
    });
    if(this.scheduledRideDate) {
      let loading = this.loadingCtrl.create({
        content: 'Saving your ride...',
        duration: 3000
      });
      // loading.present();

      let user = firebase.auth().currentUser;
      let ref = firebase.database().ref('users/'+user.uid);
      this.users$ = this.af.database.object('users/' + user.uid, { preserveSnapshot: true });
      this.users$.first().subscribe(snap => {
        let numberOfScheduledRides = snap.val().numberOfScheduledRides;
        console.log(' scheduled rides '); console.log(numberOfScheduledRides);
        if(numberOfScheduledRides) {
          let totalRides = parseInt(numberOfScheduledRides) + 1;

          ref.update({
            numberOfScheduledRides: totalRides
          }).then(() => {
            ref.child('scheduledRides').child(totalRides.toString()).update({

              pickup: this.address_pickup.place,
              dropOff: this.address_dropoff.place,
              distance: this.distance,
              duration: this.duration,
              estimate: this.estimate
            }).then(() => {
              save.present();
            }).catch(err => console.log);
          }).catch(err => console.log);
        } else {
          ref.update({
            numberOfScheduledRides: '1'
          }).then(() => {
            ref.child('scheduledRides').child('1').update({
              pickup: this.address_pickup.place,
              dropOff: this.address_dropoff.place,
              distance: this.distance,
              duration: this.duration,
              estimate: this.estimate,
              dateAndTime: this.scheduledRideDate
            }).then(() => {
              save.present();
            }).catch(err => console.log);
          }).catch(err => console.log);
        }
      });
    } else {
      let noSave = this.alertCtrl.create({
        title: "Please provide date and time",
        buttons: ['Retry']
      });
      noSave.present();
    }
  }
}
