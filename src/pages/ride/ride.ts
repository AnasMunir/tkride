import { Component, ElementRef, ViewChild, OnInit } from '@angular/core';
import { NavController, MenuController, AlertController, Platform, NavParams } from 'ionic-angular';

import { AngularFire, FirebaseObjectObservable, FirebaseListObservable } from 'angularfire2';
// import { HomePage } from '../home/home';
import { MapPage } from '../map/map';
import { GoogleMaps } from '../../providers/google-maps';
import { Geolocation, Geoposition } from 'ionic-native';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Rx';

declare var GeoFire: any;
declare var google;
declare var FCMPlugin: any;

@Component({
  selector: 'page-ride',
  templateUrl: 'ride.html'
})
export class RidePage implements OnInit {
  @ViewChild('map') mapElement: ElementRef;

  map: any;
  driverMarker: any;

  public pickupLat: Geoposition;
  public pickupLng: Geoposition;
  public driverLat: Geoposition;
  public driverLng: Geoposition;

  public driverId: any;
  drivers$: FirebaseObjectObservable<any>;
  users$: FirebaseObjectObservable<any>;


  constructor(public navCtrl: NavController,
    public menu: MenuController,
    public maps: GoogleMaps,
    public platform: Platform,
    public af: AngularFire,
    public navParams: NavParams,
    public alertCtrl: AlertController) {

    this.pickupLat = this.navParams.get('pickupLat');
    this.pickupLng = this.navParams.get('pickupLng');
    this.driverId = this.navParams.get('driverId');

    console.log('pickupLat ' + this.pickupLat);
    console.log('pickupLng ' + this.pickupLng);
    console.log('driver uid: ' + this.driverId);

    let ref = firebase.database().ref('drivers/locations/' + this.driverId);

    ref.on("value", (snap) => {
      let val = snap.val();
      this.driverLat = val.l[0];
      this.driverLng = val.l[1];
      console.log('driverLat ' + this.driverLat);
      console.log('driverLng ' + this.driverLng);
    });

    console.log('driverLat ' + this.driverLat);
    console.log('driverLng ' + this.driverLng);

    var directionsService = new google.maps.DirectionsService;
    var directionsDisplay = new google.maps.DirectionsRenderer({suppressMarkers: true});;

    Geolocation.getCurrentPosition().then((pos) => {
      console.log('pos :' + pos);
      let latLng = new google.maps.LatLng(this.pickupLat, this.pickupLng);
      let driverLatLng = new google.maps.LatLng(this.driverLat, this.driverLng);

      let mapOptions = {
        center: latLng,
        scroll: true,
        rotate: true,
        myLocationButton: true,
        indoorPicker: true,
        zoom: 15,
        compass: true,
        bearing: 50,
        tilt: 30,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }

      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
      let driverImage = {
        url: 'assets/img/car.png',
        size: new google.maps.Size(100, 100),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(50, 50)
      };
      let personImage = {
        url: 'assets/img/person.png',
        size: new google.maps.Size(100, 100),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(50, 50)
      };
      this.driverMarker = new google.maps.Marker({
        map: this.map,
        animation: null,
        position: driverLatLng,
        icon: driverImage
      });
      let personMarker = new google.maps.Marker({
        map: this.map,
        animation: null,
        position: latLng,
        icon: personImage
      });

      directionsDisplay.setMap(this.map);

      directionsService.route({
        origin: latLng,
        destination: driverLatLng,
        travelMode: 'DRIVING'
      }, function (response, status) {
        if (status === 'OK') {
          directionsDisplay.setDirections(response);
        } else {
          console.log('Directions request failed due to ' + status);
        }
      });

    });

    ref.on("value", (snap) => {
      let val = snap.val();
      // this.driverLat = val.l[0];
      // this.driverLng = val.l[1];
      let markerPos = new google.maps.LatLng(val.l[0], val.l[1]);
      this.driverMarker.setPosition(markerPos);

    });

    let arrived = this.alertCtrl.create({
      title: 'Driver has arrived!',
      message: 'The driver has arrived to the pick up location',
      buttons: [
        {
          text: 'Ok',
        }
      ]
    });

    let driverRef = firebase.database().ref('drivers/locations');
    driverRef.on("value", (snap) => {
      console.log(snap.val());
    });

    this.drivers$ = this.af.database.object('drivers/');

    let geoFire = new GeoFire(driverRef);
    let geoQuery = geoFire.query({
      center: [this.pickupLat, this.pickupLng],
      radius: 0.5
    });

    geoQuery.on("key_entered", key => {
      this.drivers$.first().subscribe(snapshot => {
        console.log("Driver has arrived to the pickup location");
        console.log(snapshot[key]);
        arrived.present();
      });
    });
    let self = this;
    FCMPlugin.onNotification(
      (data) => {
        if (data.wasTapped) {
          console.log(data);
          let driverId = data.driverId;
          let distance = data.distance;
          // let points = distance.replace("mi", "points");

          if (JSON.parse(data.response) === true) {
            let alert = this.alertCtrl.create({
              title: 'Driver has started your ride',
              // subTitle: 'Points: ' + points,
              buttons: [
              {
                text: 'OK',
                handler: () => {
                  console.log('OK clicked');
                }
              },
            ]
             })
            alert.present();
          } else {
            let alert = this.alertCtrl.create({
              title: 'Driver has ended your ride',
              // subTitle: 'Points: ' + points,
              buttons: [
              {
                text: 'OK',
                handler: () => {
                  console.log('ended ride clicked');
                  let navTransition = alert.dismiss();
                  navTransition.then(() => {
                    this.navCtrl.setRoot(MapPage);
                  });
                }
              },
            ]
             })
            alert.present();
          }
        } else {
          console.log(data);
          let driverId = data.driverId;
          let distance = data.distance;
          // let points = distance.replace("mi", "points");

          if (JSON.parse(data.response) === true) {
            let alert = this.alertCtrl.create({
              title: 'Driver has started your ride',
              // subTitle: 'Points: ' + points,
              buttons: [
              {
                text: 'OK',
                handler: () => {
                  console.log('OK clicked');
                }
              },
            ]
             })
            alert.present();
          } else {
            let alert = this.alertCtrl.create({
              title: 'Driver has ended your ride',
              // subTitle: 'Points: ' + points,
              buttons: [
              {
                text: 'OK',
                handler: () => {
                  console.log('ended ride clicked');
                  let navTransition = alert.dismiss();
                  navTransition.then(() => {
                    this.navCtrl.setRoot(MapPage);
                  });
                }
              },
            ]
             })
            alert.present();
          }
        }
      },
      (msg) => {
        console.log('onNotification callback successfully registered: ' + msg);
      },
      (err) => {
        console.log('Error registering onNotification callback: ' + err);
      }
    );
    // this.rideStart(this.pickupLat, this.pickupLng);
  }

  ionViewDidLoad() {
    console.log('Hello RidePage Page');
    // this.menu.enable(true);
  }
  rideStart(pickupLat, pickupLng) {

    let ref = firebase.database().ref('drivers/locations');
    ref.on("value", (snap) => {
      console.log(snap.val());
    });

    this.drivers$ = this.af.database.object('drivers/');

    let geoFire = new GeoFire(ref);
    let geoQuery = geoFire.query({
      center: [pickupLat, pickupLng],
      radius: 0.5
    });

    geoQuery.on("key_entered", key => {
      this.drivers$.first().subscribe(snapshot => {
        console.log("Driver has arrived to the pickup location");
        console.log(snapshot[key]);
      });
    });

  }
  ngOnInit() {

  }

}
