import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

/*
  Generated class for the IncentivePrograms page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-incentive-programs',
  templateUrl: 'incentive-programs.html'
})
export class IncentiveProgramsPage {

  constructor(public navCtrl: NavController) {}

  ionViewDidLoad() {
    console.log('Hello IncentiveProgramsPage Page');
  }

}
